package at.lumi200.sleeperagent.container;

public enum ControlSign {
	NEW_LINE, BUCHSTABE, ZIFFER, ZWISCHENRAUM, CODE
}
