package at.lumi200.sleeperagent.container;

public class Element {
	private String letter = null;
	private String number = null;
	private ControlSign controlSign = null;
	private NonSemiMachineCode nonSemiMachineCode = null;

	
	public Element(String letter) {
		this.setLetter(letter);
	}
	
	public Element(String letter, String number) {
		this(letter);
		this.setNumber(number);
	}
	
	public Element(ControlSign controlSign) {
		this.setControlSign(controlSign);
	}
	
	public Element(NonSemiMachineCode nonSemiMachineCode) {
		this.setNonSemiMachineCode(nonSemiMachineCode);
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public ControlSign getControlSign() {
		return controlSign;
	}

	public void setControlSign(ControlSign controlSign) {
		this.controlSign = controlSign;
	}

	public NonSemiMachineCode getNonSemiMachineCode() {
		return nonSemiMachineCode;
	}

	public void setNonSemiMachineCode(NonSemiMachineCode nonSemiMachineCode) {
		this.nonSemiMachineCode = nonSemiMachineCode;
	}
}
