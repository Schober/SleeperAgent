package at.lumi200.sleeperagent.container;

public enum ElementType {
	BUCHSTABE, ZIFFER, CODE;
}
