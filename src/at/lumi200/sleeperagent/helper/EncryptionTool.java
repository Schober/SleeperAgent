package at.lumi200.sleeperagent.helper;

import at.lumi200.sleeperagent.container.OperationMode;

public class EncryptionTool {

    private static String encrypt(String zwt, String ar) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < zwt.length(); i++) {
            output.append((getSignleDigitFromString(zwt, i) + getSignleDigitFromString(ar, i)) % 10);
        }
        return output.toString();
    }

    public static String convert(OperationMode mode, String zwt, String ar) {
        switch (mode) {
            case ENCRYPT : return  encrypt(zwt, ar);
            case DECRYPT : return  decrypt(zwt, ar);
            default: return "ERROR: mode doesn't exist";
        }
    }

    private static String decrypt(String zwt, String ar) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < zwt.length(); i++) {
            output.append((getSignleDigitFromString(zwt, i) + 10 - getSignleDigitFromString(ar, i)) % 10);
        }
        return output.toString();

    }

    private static int getSignleDigitFromString(String inputText, int intAt) {
        return Integer.parseInt(String.valueOf(inputText.charAt(intAt)));
    }
}