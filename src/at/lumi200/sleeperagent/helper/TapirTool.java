package at.lumi200.sleeperagent.helper;

import java.util.HashMap;
import java.util.Map;

import at.lumi200.sleeperagent.container.ControlSign;
import at.lumi200.sleeperagent.container.Element;
import at.lumi200.sleeperagent.container.ElementType;
import at.lumi200.sleeperagent.container.NonSemiMachineCode;

public class TapirTool {

	// TODO
	public static String encode(String input) {
		String out = "";
		return out;
	}

	/*-
	 * TAPIR VVS-Ex. 03086
	 *
	 * |  A  /  0  |  E  /  1  |  I  /  2  |  N  /  3  |  R  /  4  |
	 * |  B  / 50  |  BE / 51  |  C  / 52  |  CH / 53  |  D  / 54  |  DE / 55  |  F  / 56  |  G  / 57  |  GE / 58  |  H  / 59  |
	 * |  J  / 60  |  K  / 61  |  L  / 62  |  M  / 63  |  O  / 64  |  -- / 65  |  -- / 66  |  P  / 67  |  Q  / 68  |  S  / 69  |
	 * |  T  / 70  |  TE / 71  |  U  / 72  |  UN / 73  |  V  / 74  |  -- / 75  |  W  / 76  |  X  / 77  |  Y  / 78  |  Z  / 79  |
	 * |  WR / 80  |  Bu / 81  |  Zi / 82  | ZwR / 83  | Code / 84 | RPT / 85  |  -- / 86  |  -- / 87  |  -- / 88  |  •  / 89  |
	 * |  :  / 90  |  ,  / 91  |  -  / 92  |  /  / 93  |  (  / 94  |  )  / 95  |  +  / 96  |  =  / 97  |  "  / 98  |  -- / 99  |
	 * |  0  / 00  |  1  / 11  |  2  / 22  |  3  / 33  |  4  / 44  |  5  / 55  |  6  / 66  |  7  / 77  |  8  / 88  |  9  / 99  |
	 * Source: http://scz.bplaced.net/m.html#t
	 */
	private static Map<String, Element> getTAPIRVVSEx03086() {
		Map<String, Element> map = new HashMap<>();
		map.put("0", new Element("A"));
		map.put("1", new Element("E"));
		map.put("2", new Element("I"));
		map.put("3", new Element("N"));
		map.put("4", new Element("R"));
		map.put("50", new Element("B"));
		map.put("51", new Element("BE"));
		map.put("52", new Element("C"));
		map.put("53", new Element("CH"));
		map.put("54", new Element("D"));
		map.put("55", new Element("DE", "5"));
		map.put("56", new Element("F"));
		map.put("57", new Element("G"));
		map.put("58", new Element("GE"));
		map.put("59", new Element("H"));
		map.put("60", new Element("J"));
		map.put("61", new Element("K"));
		map.put("62", new Element("L"));
		map.put("63", new Element("M"));
		map.put("64", new Element("O"));
		map.put("67", new Element("P"));
		map.put("68", new Element("Q"));
		map.put("69", new Element("S"));
		map.put("70", new Element("T"));
		map.put("71", new Element("TE"));
		map.put("72", new Element("U"));
		map.put("73", new Element("UN"));
		map.put("74", new Element("V"));
		map.put("76", new Element("W"));
		map.put("77", new Element("X", "7"));
		map.put("78", new Element("Y"));
		map.put("79", new Element("Z"));
		map.put("80", new Element(ControlSign.NEW_LINE));
		map.put("81", new Element(ControlSign.BUCHSTABE));
		map.put("82", new Element(ControlSign.ZIFFER));
		map.put("83", new Element(ControlSign.ZWISCHENRAUM));
		map.put("84", new Element(ControlSign.CODE));
		map.put("85", new Element(NonSemiMachineCode.REPEAT));
		map.put("89", new Element(null, "."));
		map.put("90", new Element(null, ":"));
		map.put("91", new Element(null, ","));
		map.put("92", new Element(null, "-"));
		map.put("93", new Element(null, "/"));
		map.put("94", new Element(null, "("));
		map.put("95", new Element(null, ")"));
		map.put("96", new Element(null, "+"));
		map.put("97", new Element(null, "="));
		map.put("98", new Element(null, "\""));
		map.put("00", new Element(null, "0"));
		map.put("11", new Element(null, "1"));
		map.put("22", new Element(null, "2"));
		map.put("33", new Element(null, "3"));
		map.put("44", new Element(null, "4"));
		map.put("66", new Element(null, "6"));
		map.put("88", new Element(null, "8"));
		map.put("99", new Element(null, "9"));
		return map;
	}

	public static String decodeTAPIRVVSEx03086(String input) {
		Map<String, Element> map = getTAPIRVVSEx03086();
		ElementType elementType = ElementType.BUCHSTABE;
		StringBuilder stb = new StringBuilder();

		for (int i = 0; i < input.length() - 1; i++) {
			String value = String.valueOf(input.charAt(i));
			Element decodedValue = map.get(value);

			// Check if one digit is valid
			if (decodedValue == null) {
				// If not -> use two digits
				i++;
				decodedValue = map.get(value + String.valueOf(input.charAt(i)));
			}

			if (decodedValue.getControlSign() != null) {
				switch (decodedValue.getControlSign()) {
				case NEW_LINE -> stb.append(System.getProperty("line.separator"));
				case BUCHSTABE -> elementType = ElementType.BUCHSTABE;
				case ZIFFER -> elementType = ElementType.ZIFFER;
				case ZWISCHENRAUM -> {
					// TODO: Is this how it should be?
					if (elementType.equals(ElementType.CODE)) {
						elementType = ElementType.BUCHSTABE;
						stb.append("«");
					}
					stb.append(" ");
				}
				case CODE -> {
					if (!elementType.equals(ElementType.CODE)) {
						elementType = ElementType.CODE;
						stb.append("»");
					}
				}
				default -> throw new IllegalArgumentException("Unexpected value: " + decodedValue.getControlSign());
				}
			} else if (decodedValue.getNonSemiMachineCode() != null) {
				stb.append("<");
				stb.append(decodedValue.getNonSemiMachineCode());
				stb.append(">");
			} else {
				switch (elementType) {
				case CODE:
					stb.append(decodedValue.getLetter());
					break;

				case BUCHSTABE:
					stb.append(decodedValue.getLetter());
					break;

				case ZIFFER:
					stb.append(decodedValue.getNumber());
					break;

				default:
					throw new IllegalArgumentException("Unexpected value: " + elementType);
				}
			}
		}

		return stb.toString();
	}
}
