package at.lumi200.sleeperagent.helper;

import at.lumi200.sleeperagent.container.OperationMode;

import java.util.Scanner;

public class CLITool {
    private static final Scanner scan = new Scanner(System.in);

    public static OperationMode encryptOrDecrypt() {


        System.out.println("Substitutionstext Chiffrieren oder Dechiffrieren (C/D) ");
        switch (scan.nextLine().toUpperCase()) {
            case "C":
                return OperationMode.ENCRYPT;
            case "D":
                return OperationMode.DECRYPT;
            default:
                return OperationMode.ENCRYPT;
        }
    }

    public static String getSubstitutionText() {
        System.out.println("Zwischentext:");
        String zwtRaw = scan.nextLine();
        zwtRaw = zwtRaw.replaceAll("[- ]", "");
        return zwtRaw;


    }

    public static String getOTP() {
        System.out.println("Additionsreihe:");
        String arRaw = scan.nextLine();
        arRaw = arRaw.replaceAll("[- ]", "");
        return arRaw;
    }

    public static void printResult(OperationMode mode, String output) {
        switch (mode) {
            case ENCRYPT:
                System.out.println("Chiffrentext:");
                System.out.println(output.replaceAll(".{5}", "$0 "));
                break;
            case DECRYPT:
                System.out.println("Klartext:");
                System.out.println(output.replaceAll(".{5}", "$0 "));
                System.out.print(TapirTool.decodeTAPIRVVSEx03086(output));
                break;
            default: System.out.println("ERROR: mode doesn't exist");
        }

    }


}
