package at.lumi200.sleeperagent.tests;

import at.lumi200.sleeperagent.helper.TapirTool;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class TapirToolTest {

    @Test
    @Disabled
    void encode() {
        //NYI
    }

    @Test
    void decodeTAPIRVVSEx03086() {
        /*
        Substitution text: 5906262648291818382558155
        Text:              "Hallo, 5DE"
         */
        Assertions.assertEquals("HALLO, 5DE", TapirTool.decodeTAPIRVVSEx03086("5906262648291818382558155"));
    }
}