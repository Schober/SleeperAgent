package at.lumi200.sleeperagent.tests;

import at.lumi200.sleeperagent.container.OperationMode;
import at.lumi200.sleeperagent.helper.EncryptionTool;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EncryptionToolTest {

    @Test
    @DisplayName("Encrypt substitution text")
    void encrypt() {
        /* Following text will be the test samples
        ZWT 12345 64574
        AR  78356 37723
        OUT 80691 91297
         */
        Assertions.assertEquals("8069191297", EncryptionTool.convert(OperationMode.ENCRYPT, "1234564574", "7835637723"));
    }

    @Test
    @DisplayName("Decrypt substitution text")
    void decrypt() {
         /* Following text will be the test samples
        ZWT 80691 91297
        AR  78356 37723
        OUT 12345 64574
         */
        Assertions.assertEquals("1234564574", EncryptionTool.convert(OperationMode.DECRYPT, "8069191297", "7835637723"));

    }
}