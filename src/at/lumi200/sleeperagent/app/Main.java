package at.lumi200.sleeperagent.app;

import at.lumi200.sleeperagent.container.OperationMode;
import at.lumi200.sleeperagent.helper.CLITool;
import at.lumi200.sleeperagent.helper.EncryptionTool;

public class Main {

    public static void main(String[] args) {


        OperationMode mode = CLITool.encryptOrDecrypt();

        CLITool.printResult(mode, EncryptionTool.convert(mode, CLITool.getSubstitutionText(), CLITool.getOTP()));


    }
}
